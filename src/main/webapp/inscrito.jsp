<%-- 
    Document   : index
    Created on : 04-04-2021, 14:23:30
    Author     : JSuarez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro alumno y sección</title>
    </head>
    <style>
        html, body { min-height:97vh; position:relative; margin:0; padding:0; }
        footer { position:absolute; bottom:0px; width:100%; }
        footer p { text-align:center; }
    </style>
    <body> 
    <center>
        <form name="form" action="/inscrito.jsp" method="GET"> 
            <h1> ¡Aquí podrás registrarte como alumno! </h1>
            <br>   
            <label for="nombre">Nombre del alumno: </label>
            <br>
            <input type "text" id="nombre" name="nombre" required>
            <br>     <br>
            <label for="valor1">Ingrese grupo o seccion: </label>     <br>
            <input type "text" id="seccion" name="seccion" required>
            <br> <br>     <br>     <br>
            <button type="submit" class="btn btn-success"> Inscribirse </button> 

        </form>
    </center>
    <footer>
        <p>Ningún derecho reservado - copyleft 2021</p>
    </footer>
</body>
</html>

